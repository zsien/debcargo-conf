Source: rust-servo-fontconfig
Section: rust
Priority: optional
Build-Depends: debhelper (>= 11),
 dh-cargo (>= 18),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-libc-0.2+default-dev <!nocheck>,
 librust-servo-fontconfig-sys-5+default-dev (>= 5.1.0-~~) <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Fabio Rafael da Rosa <fdr@fabiodarosa.org>
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/servo-fontconfig]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/servo-fontconfig

Package: librust-servo-fontconfig-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-libc-0.2+default-dev,
 librust-servo-fontconfig-sys-5+default-dev (>= 5.1.0-~~)
Suggests:
 librust-servo-fontconfig+force-system-lib-dev (= ${binary:Version})
Provides:
 librust-servo-fontconfig+default-dev (= ${binary:Version}),
 librust-servo-fontconfig-0-dev (= ${binary:Version}),
 librust-servo-fontconfig-0+default-dev (= ${binary:Version}),
 librust-servo-fontconfig-0.5-dev (= ${binary:Version}),
 librust-servo-fontconfig-0.5+default-dev (= ${binary:Version}),
 librust-servo-fontconfig-0.5.1-dev (= ${binary:Version}),
 librust-servo-fontconfig-0.5.1+default-dev (= ${binary:Version})
Description: Rust bindings for fontconfig - Rust source code
 This package contains the source for the Rust servo-fontconfig crate, packaged
 by debcargo for use with cargo and dh-cargo.

Package: librust-servo-fontconfig+force-system-lib-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-servo-fontconfig-dev (= ${binary:Version}),
 librust-servo-fontconfig-sys-5+force-system-lib-dev (>= 5.1.0-~~)
Provides:
 librust-servo-fontconfig-0+force-system-lib-dev (= ${binary:Version}),
 librust-servo-fontconfig-0.5+force-system-lib-dev (= ${binary:Version}),
 librust-servo-fontconfig-0.5.1+force-system-lib-dev (= ${binary:Version})
Description: Rust bindings for fontconfig - feature "force_system_lib"
 This metapackage enables feature "force_system_lib" for the Rust servo-
 fontconfig crate, by pulling in any additional dependencies needed by
 that feature.
